package com.vetclinic.controller;

import com.vetclinic.object.KindOfPet;
import com.vetclinic.object.KindOfService;
import com.vetclinic.object.Service;
import com.vetclinic.object.dto.ServiceDto;
import com.vetclinic.service.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/vet_clinic/admin")
public class AdminController {

    private final ServicesService servicesService;

    @Autowired
    public AdminController(ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    @GetMapping
    public String showAdminPage() {
        return "admin/admin.html";
    }

    @GetMapping("/add_kind")
    public String showAddKindPage(Model model) {
        model.addAttribute("kind", new KindOfPet());

        return "admin/addKindOfService.html";
    }

    @GetMapping("/add_service")
    public String showAddServicePage(Model model) {
        model.addAttribute("service", new ServiceDto());
        model.addAttribute("kinds", servicesService.getKinds());

        return "admin/addService.html";
    }

    @PostMapping("/add_kind")
    public String addKind(@ModelAttribute("kind") KindOfService kindOfService) {
        servicesService.createKind(kindOfService);

        return "redirect:/vet_clinic/admin/add_kind";
    }

    @PostMapping("/add_service")
    public String addService(@ModelAttribute("service") ServiceDto serviceDto) {
        servicesService.createService(serviceDto);

        return "redirect:/vet_clinic/admin/add_service";
    }

    @GetMapping("/get_kind")
    @ResponseBody
    public KindOfService getKind(@RequestParam int id) {
        return servicesService.getKindOfServiceById(id);
    }

    @PostMapping("/update_kind")
    public String updateKind(@ModelAttribute("kind") KindOfService kindOfService) {
        servicesService.updateKind(kindOfService);

        return "redirect:/vet_clinic/admin/update_kind";
    }

    @GetMapping("/update_kind")
    public String showUpdateKindPage(Model model) {
        model.addAttribute("kind", new KindOfService());
        model.addAttribute("kinds", servicesService.getKinds());

        return "admin/updateKindOfService.html";
    }

    @GetMapping("/get_service")
    @ResponseBody
    public Service getService(@RequestParam int id) {
        return servicesService.getServiceById(id);
    }

    @PostMapping("/update_service")
    public String updateService(@ModelAttribute("service") ServiceDto serviceDto) {
        servicesService.updateService(serviceDto);

        return "redirect:/vet_clinic/admin/update_service";
    }

    @GetMapping("/update_service")
    public String showUpdateServicePage(Model model) {
        model.addAttribute("service", new ServiceDto());
        model.addAttribute("services", servicesService.getServicesReal());
        model.addAttribute("kinds", servicesService.getKinds());

        return "admin/updateService.html";
    }

    @PostMapping("/delete_kind")
    public String deleteKind(@ModelAttribute("kind") KindOfService kindOfService) {
        servicesService.deleteKind(kindOfService.getId());

        return "redirect:/vet_clinic/admin/delete_kind";
    }

    @GetMapping("/delete_kind")
    public String showDeleteKindPage(Model model) {
        model.addAttribute("kind", new KindOfService());
        model.addAttribute("kinds", servicesService.getKinds());

        return "admin/deleteKindOfService.html";
    }
}
