package com.vetclinic.controller;

import com.vetclinic.object.Feedback;
import com.vetclinic.object.KindOfService;
import com.vetclinic.object.Page;
import com.vetclinic.object.PageRequest;
import com.vetclinic.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/vet_clinic")
public class MailController {

    private final static int PAGE_SIZE = 5;
    private final FeedbackService feedbackService;

    @Autowired
    public MailController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @GetMapping
    public String showMainPage() {
        return "main";
    }

    @GetMapping("/get_feedbacks")
    @ResponseBody
    public Page<Feedback> getFeedbacks(@RequestParam(defaultValue = "1") int page) {
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(page);
        pageRequest.setPerPage(PAGE_SIZE);

        return feedbackService.getFeedbacks(pageRequest);
    }

    @PostMapping("/add_feedback")
    public ResponseEntity<String> addFeedback(@RequestBody Feedback feedback,
                                              @AuthenticationPrincipal UserDetails userDetails) {

        feedbackService.createFeedback(feedback, userDetails.getUsername());

        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/veterinarian")
    public String showVetPage() {
        return "vet/vet.html";
    }
}
