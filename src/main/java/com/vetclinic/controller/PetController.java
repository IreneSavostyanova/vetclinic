package com.vetclinic.controller;

import com.vetclinic.object.Breed;
import com.vetclinic.object.Pet;
import com.vetclinic.object.dto.PetDto;
import com.vetclinic.service.BreedService;
import com.vetclinic.service.KindOfPetService;
import com.vetclinic.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vet_clinic")
public class PetController {

    private final PetService petService;
    private final BreedService breedService;
    private final KindOfPetService kindOfPetService;

    @Autowired
    public PetController(PetService petService,
                         BreedService breedService,
                         KindOfPetService kindOfPetService) {

        this.petService = petService;
        this.breedService = breedService;
        this.kindOfPetService = kindOfPetService;
    }

    @GetMapping("/add_pet")
    public String showPageForAddPet(Model model) {
        model.addAttribute("pet", new PetDto());

        return "user/addPet.html";
    }

    @PostMapping("/add_pet")
    public String addPet(@AuthenticationPrincipal UserDetails userDetails,
                         @ModelAttribute("pet") @Valid PetDto pet,
                         BindingResult bindingResult,
                         Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("pet", pet);

            return "user/addPet.html";
        }
        petService.addPet(userDetails.getUsername(), pet);

        return "redirect:/vet_clinic";
    }

    @GetMapping("/add_pet/get_breeds")
    @ResponseBody
    public List<Breed> getBreeds(@RequestParam() int id) {
        return breedService.getBreedsByKindOfPetId(id);
    }

    @GetMapping("/my_pets")
    public String showMyPats() {

        return "user/pets.html";
    }

    @GetMapping("/get_my_pets")
    @ResponseBody
    public List<Pet> getMyPats(@AuthenticationPrincipal UserDetails userDetails) {

        return petService.getUserPets(userDetails.getUsername());
    }

    @ModelAttribute
    public void addAttribute(Model model) {
        model.addAttribute("kindsOfPet", kindOfPetService.getKindsOfPet());
    }

    @GetMapping("/update_pet")
    public String showUpdatePage(@RequestParam() int id, Model model) {
        model.addAttribute("pet", petService.getPetById(id));

        return "user/updatePet.html";
    }

    @PostMapping("/update_pet")
    public String updatePat(@ModelAttribute("pet") @Valid PetDto pet,
                            BindingResult bindingResult,
                            Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("pet", pet);

            return "user/updatePet.html";
        }
        petService.updatePet(pet);

        return "redirect:/vet_clinic/my_pets";
    }

    @PostMapping("/delete_pet")
    @ResponseBody
    public void deletePet(@RequestParam int id) {
        petService.deletePetById(id);
    }
}
