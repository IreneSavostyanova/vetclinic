package com.vetclinic.controller;

import com.vetclinic.object.dto.ReceptionDto;
import com.vetclinic.service.ReceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller()
@RequestMapping("/vet_clinic")
public class ReceptionController {

    private final ReceptionService receptionService;

    @Autowired
    public ReceptionController(ReceptionService receptionService) {
        this.receptionService = receptionService;
    }

    @PostMapping("/veterinarian/create_reception")
    @ResponseBody
    public ResponseEntity<String> createReception(@RequestBody ReceptionDto receptionDto,
                                                  @AuthenticationPrincipal UserDetails userDetails) {

        receptionService.createReception(userDetails.getUsername(), receptionDto);

        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/veterinarian/get_meetings")
    @ResponseBody
    public List<ReceptionDto> getReceptions(@AuthenticationPrincipal UserDetails userDetails) {
        return receptionService.getReceptionForVeterinarian(userDetails.getUsername());
    }

    @GetMapping("/veterinarian/get_meetings_tomorrow")
    @ResponseBody
    public List<ReceptionDto> getReceptionsTomorrow(@AuthenticationPrincipal UserDetails userDetails) {
        return receptionService.getReceptionForVeterinarianTomorrow(userDetails.getUsername());
    }

    @GetMapping("/veterinarian/get_meetings_by_date")
    @ResponseBody
    public List<ReceptionDto> getReceptions(@AuthenticationPrincipal UserDetails userDetails,
                                            @RequestParam String date) {

        return receptionService.getReceptionsByDate(userDetails.getUsername(), date);
    }

    @GetMapping("/get_meetings")
    @ResponseBody
    public List<ReceptionDto> getReceptionsForUser(@AuthenticationPrincipal UserDetails userDetails) {

        return receptionService.getReceptionForUser(userDetails.getUsername());
    }

    @GetMapping("/meetings")
    public String showMeetings() {

        return "user/meetings.html";
    }

    @PostMapping("/veterinarian/mark_reception")
    @ResponseBody
    public String markReception(@RequestParam int id) {
        receptionService.markReceptionLikeDone(id);
        return "successfully";
    }

    @GetMapping("/veterinarian/report")
    public String showReportPage() {
        return "vet/report.html";
    }
}
