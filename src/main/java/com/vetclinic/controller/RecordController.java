package com.vetclinic.controller;

import com.vetclinic.object.Pet;
import com.vetclinic.object.Service;
import com.vetclinic.object.dto.RecordDto;
import com.vetclinic.object.dto.ServiceDto;
import com.vetclinic.service.PetService;
import com.vetclinic.service.RecordService;
import com.vetclinic.service.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class RecordController {

    private final ServicesService servicesService;
    private final PetService petService;
    private final RecordService recordService;

    @Autowired
    public RecordController(ServicesService servicesService,
                            PetService petService,
                            RecordService recordService) {

        this.servicesService = servicesService;
        this.petService = petService;
        this.recordService = recordService;
    }

    @GetMapping("/vet_clinic/record")
    public String showRecordPage(Model model) {
        model.addAttribute("record", new RecordDto());

        return "user/record.html";
    }

    @PostMapping("/vet_clinic/record")
    @ResponseBody
    public ResponseEntity<String> saveOrder(@RequestBody Service[] services,
                                            @RequestParam int petId) {

        recordService.createRecord(petId, services);

        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping("/vet_clinic/record/get_services")
    @ResponseBody
    public List<ServiceDto> getServices() {
        return servicesService.getServices();
    }

    @GetMapping("/vet_clinic/record/get_user_pets")
    @ResponseBody
    public List<Pet> getPets(@AuthenticationPrincipal UserDetails userDetails) {
        return petService.getUserPets(userDetails.getUsername());
    }

    @GetMapping("/vet_clinic/veterinarian/records/get_records")
    @ResponseBody
    public List<RecordDto> getRecords() {
        return recordService.getRecords();
    }

    @GetMapping("/vet_clinic/veterinarian/records")
    public String showRecords() {
        return "vet/records.html";
    }


}
