package com.vetclinic.controller;

import com.vetclinic.object.Service;
import com.vetclinic.service.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/vet_clinic/services")
public class ServiceController {

    private final ServicesService servicesService;

    @Autowired
    public ServiceController(ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    @GetMapping()
    public String showServices(Model model) {
        model.addAttribute("services", servicesService.getServices());

        return "user/services.html";
    }

    @GetMapping("/get_services_by_term")
    @ResponseBody
    public List<Service> getServicesByTerm(@RequestParam String term) {

        return servicesService.getServicesLikeTerm(term);
    }
}
