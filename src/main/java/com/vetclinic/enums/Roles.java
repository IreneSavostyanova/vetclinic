package com.vetclinic.enums;

public enum Roles {
    USER, ADMIN, VETERINARIAN
}
