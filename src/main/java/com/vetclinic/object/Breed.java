package com.vetclinic.object;

public class Breed {

    private int id;
    private KindOfPet kindOfPet;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public KindOfPet getKindOfPet() {
        return kindOfPet;
    }

    public void setKindOfPet(KindOfPet kindOfPet) {
        this.kindOfPet = kindOfPet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
