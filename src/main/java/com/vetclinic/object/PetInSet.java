package com.vetclinic.object;

public class PetInSet {

    private int id;
    private PetSet petSet;
    private Pet pet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PetSet getPetSet() {
        return petSet;
    }

    public void setPetSet(PetSet petSet) {
        this.petSet = petSet;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
