package com.vetclinic.object.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PetDto {

    private int id;

    @NotEmpty
    @NotNull
    private String nickname;

    @NotEmpty
    @NotNull
    private String dob;

    @NotNull
    private int breedId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getBreedId() {
        return breedId;
    }

    public void setBreedId(int breedId) {
        this.breedId = breedId;
    }
}
