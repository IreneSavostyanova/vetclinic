package com.vetclinic.object.dto;

import com.vetclinic.object.User;

public class ReceptionDto {

    private int id;
    private int recordId;
    private String date;
    private String time;
    private User veterinarian;
    private RecordDto recordDto;
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public User getVeterinarian() {
        return veterinarian;
    }

    public void setVeterinarian(User veterinarian) {
        this.veterinarian = veterinarian;
    }

    public RecordDto getRecordDto() {
        return recordDto;
    }

    public void setRecordDto(RecordDto recordDto) {
        this.recordDto = recordDto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
