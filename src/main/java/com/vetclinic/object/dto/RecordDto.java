package com.vetclinic.object.dto;

import com.vetclinic.object.Pet;
import com.vetclinic.object.Service;
import com.vetclinic.object.User;

import java.util.List;

public class RecordDto {

    private int recordId;
    private int petId;
    private List<Service> services;
    private List<Integer> idOfServices;
    private User user;
    private Pet pet;
    private double priceSum;

    public double getPriceSum() {
        for (Service service : services) {
            priceSum += service.getPrice();
        }
        return priceSum;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public int getPetId() {
        return petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public List<Integer> getIdOfServices() {
        return idOfServices;
    }

    public void setIdOfServices(List<Integer> idOfServices) {
        this.idOfServices = idOfServices;
    }
}
