package com.vetclinic.object.dto;

public class ServiceDto {

    private int id;
    private String name;
    private String kindOfServiceName;
    private String kindOfServiceDescription;
    private double price;
    private int kindOfServiceId;

    public int getKindOfServiceId() {
        return kindOfServiceId;
    }

    public void setKindOfServiceId(int kindOfServiceId) {
        this.kindOfServiceId = kindOfServiceId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKindOfServiceName() {
        return kindOfServiceName;
    }

    public void setKindOfServiceName(String kindOfServiceName) {
        this.kindOfServiceName = kindOfServiceName;
    }

    public String getKindOfServiceDescription() {
        return kindOfServiceDescription;
    }

    public void setKindOfServiceDescription(String kindOfServiceDescription) {
        this.kindOfServiceDescription = kindOfServiceDescription;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
