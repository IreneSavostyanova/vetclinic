package com.vetclinic.registration;

import com.vetclinic.service.BreedService;
import com.vetclinic.service.KindOfPetService;
import com.vetclinic.service.PetService;
import com.vetclinic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/vet_clinic/registration")
public class UserRegistrationController {

    private final UserService userService;
    private final PetService petService;

    @Autowired
    public UserRegistrationController(UserService userService, PetService petService) {
        this.userService = userService;
        this.petService = petService;
    }

    @GetMapping
    public String getRegistrationForm(Model model) {
        model.addAttribute("userRegistrationForm", new UserRegistrationForm());

        return "/registration/registration.html";
    }

    @PostMapping
    public String register(@ModelAttribute("userRegistrationForm") @Valid UserRegistrationForm userRegistrationForm,
                           BindingResult bindingResult,
                           Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("userRegistrationForm", userRegistrationForm);

            return "/registration/registration.html";
        }
        int userId = userService.register(userRegistrationForm);
        petService.createPetSetForUser(userId);

        return "redirect:/vet_clinic";
    }
}
