package com.vetclinic.repository;

import com.vetclinic.object.Breed;
import com.vetclinic.rowmapper.BreedRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreedRepository {

    private static final String SQL_GET_BREEDS_BY_ID = "SELECT * FROM breed WHERE kind_of_pet_id = :kind_of_pet_id";
    private static final String SQL_GET_BREED_BY_ID = "SELECT * FROM breed WHERE id = :id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final BreedRowMapper breedRowMapper;

    @Autowired
    public BreedRepository(NamedParameterJdbcTemplate jdbcTemplate,
                           BreedRowMapper breedRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.breedRowMapper = breedRowMapper;
    }

    public List<Breed> getBreedsByKindOfPetId(int kindOfPetId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("kind_of_pet_id", kindOfPetId);

        return jdbcTemplate.query(
                SQL_GET_BREEDS_BY_ID,
                sqlParameterSource,
                breedRowMapper
        );
    }

    public Breed getBreedById(int breedId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", breedId);
        try {
            return jdbcTemplate.queryForObject(SQL_GET_BREED_BY_ID, map, breedRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
