package com.vetclinic.repository;

import com.vetclinic.object.Feedback;
import com.vetclinic.object.PageRequest;
import com.vetclinic.rowmapper.FeedbackRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FeedbackRepository {

    private static final String SQL_GET_FEEDBACKS = "SELECT * FROM feedback ORDER BY date desc LIMIT :limit OFFSET :offset";
    private static final String SQL_CREATE_FEEDBACK = "INSERT INTO feedback (user_id, text, rank, date) " +
            "VALUES (:user_id, :text, :rank, :date)";
    private static final String SQL_GET_COUNT = "SELECT count(*) FROM feedback";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final FeedbackRowMapper feedbackRowMapper;

    @Autowired
    public FeedbackRepository(NamedParameterJdbcTemplate jdbcTemplate,
                              FeedbackRowMapper feedbackRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.feedbackRowMapper = feedbackRowMapper;
    }

    public void createFeedback(Feedback feedback) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", feedback.getUser().getId())
                .addValue("text", feedback.getText())
                .addValue("rank", feedback.getRank())
                .addValue("date", feedback.getDate());

        jdbcTemplate.update(SQL_CREATE_FEEDBACK, map);
    }

    public List<Feedback> getFeedbacks(PageRequest pageRequest) {
        int startOfLimit = pageRequest.getPage() * pageRequest.getPerPage() - pageRequest.getPerPage();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("limit", pageRequest.getPerPage())
                .addValue("offset", startOfLimit);

        return jdbcTemplate.query(SQL_GET_FEEDBACKS, sqlParameterSource, feedbackRowMapper);
    }

    public Integer getCountOfFeedbacks() {
        return jdbcTemplate.getJdbcTemplate().queryForObject(SQL_GET_COUNT, Integer.class);
    }
}
