package com.vetclinic.repository;

import com.vetclinic.object.KindOfPet;
import com.vetclinic.rowmapper.KindOfPetRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KindOfPetRepository {

    private static final String SQL_GET_KIND_BY_ID = "SELECT * FROM kind_of_pet WHERE id = :id";
    private static final String SQL_GET_KINDS = "SELECT * FROM kind_of_pet";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final KindOfPetRowMapper kindOfPetRowMapper;

    @Autowired
    public KindOfPetRepository(NamedParameterJdbcTemplate jdbcTemplate,
                               KindOfPetRowMapper kindOfPetRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.kindOfPetRowMapper = kindOfPetRowMapper;
    }

    public KindOfPet getKindOfPetById(int kindOfPetId) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", kindOfPetId);

        return jdbcTemplate.queryForObject(
                SQL_GET_KIND_BY_ID,
                sqlParameterSource,
                kindOfPetRowMapper
        );
    }

    public List<KindOfPet> getKindsOfPet() {
        return jdbcTemplate
                .query(SQL_GET_KINDS, kindOfPetRowMapper);
    }
}
