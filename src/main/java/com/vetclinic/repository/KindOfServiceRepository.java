package com.vetclinic.repository;

import com.vetclinic.object.KindOfService;
import com.vetclinic.rowmapper.KindOfServiceRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KindOfServiceRepository {

    private static final String SQL_GET_KIND_BY_ID = "SELECT * FROM kind_of_service WHERE id = :id";
    private static final String SQL_UPDATE = "UPDATE kind_of_service SET name = :name, description = :description WHERE id = :id";
    private static final String SQL_CREATE = "INSERT INTO kind_of_service (name, description) VALUES (:name, :description)";
    private static final String SQL_DELETE = "DELETE FROM kind_of_service WHERE id = :id";
    private static final String SQL_GET_ALL = "SELECT * FROM kind_of_service";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final KindOfServiceRowMapper kindOfServiceRowMapper;

    public KindOfServiceRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                   KindOfServiceRowMapper kindOfServiceRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.kindOfServiceRowMapper = kindOfServiceRowMapper;
    }

    public KindOfService getKindOfServiceById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_KIND_BY_ID, map, kindOfServiceRowMapper);
    }

    public void updateKind(KindOfService kindOfService) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", kindOfService.getId())
                .addValue("name", kindOfService.getName())
                .addValue("description", kindOfService.getDescription());

        jdbcTemplate.update(SQL_UPDATE, map);
    }

    public void createKind(KindOfService kindOfService) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("name", kindOfService.getName())
                .addValue("description", kindOfService.getDescription());

        jdbcTemplate.update(SQL_CREATE, map);
    }

    public void deleteKind(int kindOfServiceId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", kindOfServiceId);

        jdbcTemplate.update(SQL_DELETE, map);
    }

    public List<KindOfService> getKinds() {
        return jdbcTemplate.query(SQL_GET_ALL, kindOfServiceRowMapper);
    }
}
