package com.vetclinic.repository;

import com.vetclinic.object.Pet;
import com.vetclinic.object.PetSet;
import com.vetclinic.object.User;
import com.vetclinic.rowmapper.PetRowMapper;
import com.vetclinic.rowmapper.PetSetRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class PetRepository {

    private static final String SQL_CREATE_PET_SET = "INSERT INTO pet_set (user_id) VALUES (:user_id)";
    private static final String SQL_CREATE_PET = "INSERT INTO pet (nickname, dob, breed_id) " +
            "VALUES (:nickname, :dob, :breed_id)";
    private static final String SQL_ADD_PET_IN_SET = "INSERT INTO pet_in_set (pet_set_id, pet_id) " +
            "VALUES (:pet_set_id, :pet_id)";
    private static final String SQL_GET_USERS_SET = "SELECT * FROM pet_set WHERE user_id = :user_id";
    private static final String SQL_GET_USER_PETS = "SELECT p.* from pet_set\n" +
            "left join pet_in_set pis on pet_set.id = pis.pet_set_id\n" +
            "left join pet p on pis.pet_id = p.id\n" +
            "WHERE user_id = :user_id";
    private static final String SQL_GET_PET_BY_ID = "SELECT * FROM pet WHERE id = :id";
    private static final String SQL_UPDATE_PET = "UPDATE pet SET nickname = :nickname, dob = :dob WHERE id = :id";
    private static final String SQL_DELETE_PET = "DELETE FROM pet WHERE id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PetSetRowMapper petSetRowMapper;
    private final PetRowMapper petRowMapper;

    @Autowired
    public PetRepository(NamedParameterJdbcTemplate jdbcTemplate,
                         PetSetRowMapper petSetRowMapper,
                         PetRowMapper petRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.petSetRowMapper = petSetRowMapper;
        this.petRowMapper = petRowMapper;
    }

    public void createPetSetForUser(int userId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", userId);
        jdbcTemplate.update(SQL_CREATE_PET_SET, map);
    }

    public int createPet(Pet pet) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("nickname", pet.getNickname())
                .addValue("dob", pet.getDob())
                .addValue("breed_id", pet.getBreed().getId());

        jdbcTemplate.update(SQL_CREATE_PET, map, keyHolder, new String[]{"id"});

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    public void addPetToPetInSet(Pet pet, PetSet petSet) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("pet_set_id", petSet.getId())
                .addValue("pet_id", pet.getId());

        jdbcTemplate.update(SQL_ADD_PET_IN_SET, map);
    }

    public PetSet getUsersPetSet(User user) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", user.getId());

        return jdbcTemplate.queryForObject(SQL_GET_USERS_SET, map, petSetRowMapper);
    }

    public List<Pet> getUserPets(User user) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", user.getId());

        return jdbcTemplate.query(SQL_GET_USER_PETS, map, petRowMapper);
    }
    
    public Pet getPetById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);
        
        return jdbcTemplate.queryForObject(SQL_GET_PET_BY_ID, map, petRowMapper);
    }
    
    public void updatePet(Pet pet) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", pet.getId())
                .addValue("nickname", pet.getNickname())
                .addValue("dob", pet.getDob());
        jdbcTemplate.update(SQL_UPDATE_PET, sqlParameterSource);
    }

    public void deletePetById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        jdbcTemplate.update(SQL_DELETE_PET, map);
    }
}
