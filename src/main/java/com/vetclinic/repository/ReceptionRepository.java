package com.vetclinic.repository;

import com.vetclinic.object.Reception;
import com.vetclinic.object.User;
import com.vetclinic.rowmapper.ReceptionRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReceptionRepository {

    private static final String SQL_CREATE_RECEPTION = "INSERT INTO reception (record_id, veterinarian_id, date, time, done)" +
            "VALUES (:record_id, :veterinarian_id, :date, :time, :done)";
    private static final String SQL_GET_RECEPTIONS_FOR_VET = "SELECT * FROM reception " +
            "WHERE veterinarian_id = :veterinarian_id " +
            "AND TO_TIMESTAMP(date, 'YYYY-MM-DD') = CURRENT_DATE " +
            "ORDER BY date, time";
    private static final String SQL_GET_RECEPTIONS_FOR_VET_TOMORROW = "SELECT * FROM reception " +
            "WHERE veterinarian_id = :veterinarian_id " +
            "AND TO_TIMESTAMP(date, 'YYYY-MM-DD') = TIMESTAMP 'tomorrow' " +
            "ORDER BY date, time";
    private static final String SQL_GET_RECEPTIONS_FOR_VET_BY_DATE = "SELECT * FROM reception " +
            "WHERE veterinarian_id = :veterinarian_id " +
            "AND date = :date " +
            "ORDER BY time";
    private static final String SQL_GET_RECEPTIONS_FOR_USER = "SELECT reception.* FROM reception\n" +
            "left join record r on reception.record_id = r.id\n" +
            "left join pet p on r.pet_id = p.id\n" +
            "left join pet_in_set pis on p.id = pis.pet_id\n" +
            "left join pet_set ps on pis.pet_set_id = ps.id\n" +
            "WHERE user_id = :user_id AND TO_TIMESTAMP(date, 'YYYY-MM-DD') >= CURRENT_DATE";
    private static final String SQL_MARK_DONE = "UPDATE reception SET done = true WHERE id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ReceptionRowMapper receptionRowMapper;

    @Autowired
    public ReceptionRepository(NamedParameterJdbcTemplate jdbcTemplate,
                               ReceptionRowMapper receptionRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.receptionRowMapper = receptionRowMapper;
    }

    public void createReception(Reception reception) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("record_id", reception.getRecord().getId())
                .addValue("veterinarian_id", reception.getVeterinarian().getId())
                .addValue("date", reception.getDate())
                .addValue("time", reception.getTime())
                .addValue("done", false);

        jdbcTemplate.update(SQL_CREATE_RECEPTION, map);
    }

    public List<Reception> getReceptionsForVeterinarian(User veterinarian) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("veterinarian_id", veterinarian.getId());

        return jdbcTemplate.query(SQL_GET_RECEPTIONS_FOR_VET, map, receptionRowMapper);
    }

    public List<Reception> getReceptionForVeterinarianTomorrow(User veterinarian) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("veterinarian_id", veterinarian.getId());

        return jdbcTemplate.query(SQL_GET_RECEPTIONS_FOR_VET_TOMORROW, map, receptionRowMapper);
    }

    public List<Reception> getReceptionsByDate(User veterinarian, String date) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("veterinarian_id", veterinarian.getId())
                .addValue("date", date);

        return jdbcTemplate.query(SQL_GET_RECEPTIONS_FOR_VET_BY_DATE, map, receptionRowMapper);
    }

    public List<Reception> getReceptionForUser(User user) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", user.getId());

        return jdbcTemplate.query(SQL_GET_RECEPTIONS_FOR_USER, map, receptionRowMapper);
    }

    public void markReceptionLikeDone(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        jdbcTemplate.update(SQL_MARK_DONE, map);
    }
}
