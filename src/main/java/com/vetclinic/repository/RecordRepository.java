package com.vetclinic.repository;

import com.vetclinic.object.Record;
import com.vetclinic.object.Service;
import com.vetclinic.object.dto.RecordDto;
import com.vetclinic.rowmapper.RecordDtoRowMapper;
import com.vetclinic.rowmapper.RecordRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class RecordRepository {

    private static final String SQL_CREATE_RECORD = "INSERT INTO record (pet_id) VALUES (:pet_id)";
    private static final String SQL_ADD_SERVICE_IN_RECORD = "INSERT INTO services_in_record (record_id, service_id) " +
            "VALUES (:record_id, :service_id)";
    private static final String SQL_SELECT_RECORDS = "SELECT r.id, ps.user_id, p.id as pet_id from record r\n" +
            "  left join pet p on r.pet_id = p.id\n" +
            "  left join pet_in_set pis on p.id = pis.pet_id\n" +
            "  left join pet_set ps on pis.pet_set_id = ps.id\n" +
            "  WHERE NOT EXISTS(SELECT * FROM reception where record_id = r.id)\n" +
            "GROUP BY r.id, ps.user_id, p.id";
    private static final String SQL_GET_RECORD_BY_ID = "SELECT * FROM record WHERE id = :id";
    private static final String SQL_GET_RECORDDTO_BY_ID = "SELECT r.id, ps.user_id, p.id as pet_id\n" +
            "from record r\n" +
            "       left join pet p on r.pet_id = p.id\n" +
            "       left join pet_in_set pis on p.id = pis.pet_id\n" +
            "       left join pet_set ps on pis.pet_set_id = ps.id\n" +
            "WHERE r.id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RecordDtoRowMapper recordDtoRowMapper;
    private final RecordRowMapper recordRowMapper;

    @Autowired
    public RecordRepository(NamedParameterJdbcTemplate jdbcTemplate,
                            RecordDtoRowMapper recordDtoRowMapper,
                            RecordRowMapper recordRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.recordDtoRowMapper = recordDtoRowMapper;
        this.recordRowMapper = recordRowMapper;
    }

    public int createRecord(int petId) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("pet_id", petId);
        jdbcTemplate.update(SQL_CREATE_RECORD, map, keyHolder, new String[]{"id"});

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    public void addServiceInRecord(int recordId, Service service) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("record_id", recordId)
                .addValue("service_id", service.getId());
        jdbcTemplate.update(SQL_ADD_SERVICE_IN_RECORD, map);
    }

    public List<RecordDto> getRecords() {
        return jdbcTemplate.query(SQL_SELECT_RECORDS, recordDtoRowMapper);
    }


    public RecordDto getRecordDtoById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(
                SQL_GET_RECORDDTO_BY_ID,
                map,
                recordDtoRowMapper);
    }

    public Record getRecordById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_RECORD_BY_ID, map, recordRowMapper);
    }
}
