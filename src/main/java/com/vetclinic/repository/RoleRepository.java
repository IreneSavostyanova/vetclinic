package com.vetclinic.repository;

import com.vetclinic.object.Role;
import com.vetclinic.object.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class RoleRepository {

    private static final String sqlAddRole = "INSERT INTO user_role (role_id, user_id) VALUES (?, ?)";
    private static final String sqlGetRole = "SELECT * FROM role WHERE name = ?";
    private static final String sqlGetUserRoles = "SELECT r.name from users u\n" +
            "INNER JOIN user_role ur on u.id = ur.user_id\n" +
            "INNER JOIN role r on ur.role_id = r.id\n" +
            "WHERE email = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RoleRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addRoleToUser(User user, Role role) {
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(sqlAddRole);
            ps.setInt(1, role.getId());
            ps.setInt(2, user.getId());
            return ps;
        });
    }

    public Role getRoleByName(String roleName) {
        return jdbcTemplate.queryForObject(sqlGetRole, (rs, rowNum) -> {
            Role role = new Role();
            role.setId(rs.getInt("id"));
            role.setName(rs.getString("name"));
            return role;
        }, roleName);
    }

    public List<String> getUserRolesByEmail(String email) {
        return jdbcTemplate.query(
                sqlGetUserRoles, (rs, rowNum) -> rs.getString(1)
                , email
        );
    }
}
