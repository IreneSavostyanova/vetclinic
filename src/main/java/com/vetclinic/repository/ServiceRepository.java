package com.vetclinic.repository;

import com.vetclinic.object.Service;
import com.vetclinic.object.dto.ServiceDto;
import com.vetclinic.rowmapper.ServiceDtoRowMapper;
import com.vetclinic.rowmapper.ServiceRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepository {

    private static final String GET_SERVICES = "SELECT s.id, s.name, s.price, kos.name as kind_name, kos.description " +
            "FROM service s left join kind_of_service kos on s.kind_of_service_id = kos.id order by kos.name";
    private static final String SQL_GET_SERVICES_BY_RECORD = "SELECT s.* FROM service s left join services_in_record sir on s.id = sir.service_id where record_id = :record_id";
    private static final String SQL_GET_SERVICES_LIKE = "SELECT service.* FROM service " +
            "left join kind_of_service kos on service.kind_of_service_id = kos.id " +
            "WHERE LOWER(service.name) LIKE LOWER(concat(:term, '%'))";
    private static final String SQL_UPDATE = "UPDATE service SET name = :name, price = :price, kind_of_service_id = :kind_of_service_id WHERE id = :id";
    private static final String SQL_CREATE = "INSERT INTO service (name, price, kind_of_service_id) VALUES (:name, :price, :kind_of_service_id)";
    private static final String SQL_DELETE = "DELETE FROM service WHERE id = :id";
    private static final String SQL_GET_ALL = "SELECT * FROM service";
    private static final String SQL_GET_BY_ID = "SELECT * FROM service WHERE id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ServiceRowMapper serviceRowMapper;
    private final ServiceDtoRowMapper serviceDtoRowMapper;

    @Autowired
    public ServiceRepository(NamedParameterJdbcTemplate jdbcTemplate,
                             ServiceRowMapper serviceRowMapper,
                             ServiceDtoRowMapper serviceDtoRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.serviceRowMapper = serviceRowMapper;
        this.serviceDtoRowMapper = serviceDtoRowMapper;
    }


    public List<ServiceDto> getServices() {
        return jdbcTemplate.query(GET_SERVICES, serviceDtoRowMapper);
    }

    public List<Service> getServicesByRecord(int recordId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("record_id", recordId);

        return jdbcTemplate.query(SQL_GET_SERVICES_BY_RECORD, map, serviceRowMapper);
    }

    public List<Service> getServicesLikeTerm(String term) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("term", term);

        return jdbcTemplate.query(SQL_GET_SERVICES_LIKE, map, serviceRowMapper);
    }

    public void createService(Service service) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("name", service.getName())
                .addValue("price", service.getPrice())
                .addValue("kind_of_service_id", service.getKindOfService().getId());

        jdbcTemplate.update(SQL_CREATE, map);
    }

    public void updateService(Service service) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", service.getId())
                .addValue("name", service.getName())
                .addValue("price", service.getPrice())
                .addValue("kind_of_service_id", service.getKindOfService().getId());

        jdbcTemplate.update(SQL_UPDATE, map);
    }

    public void deleteService(int serviceId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", serviceId);

        jdbcTemplate.update(SQL_DELETE, map);
    }

    public List<Service> getServicesReal() {
        return jdbcTemplate.query(SQL_GET_ALL, serviceRowMapper);
    }

    public Service getServiceById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_BY_ID, map, serviceRowMapper);
    }
}
