package com.vetclinic.repository;

import com.vetclinic.object.Role;
import com.vetclinic.object.User;
import com.vetclinic.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class UserRepository {

    private static final String SQL_SAVE_USER = "INSERT INTO users (first_name, last_name, phone, email, password_hash) " +
            "VALUES (:first_name, :last_name, :phone, :email, :password_hash)";
    private static final String SQL_GET_USER = "SELECT * FROM users WHERE email = ?";
    private static final String SQL_GET_BY_ID = "SELECT * FROM users WHERE id = :id";
    private static final String SQL_ASSIGN_ADMIN = "INSERT INTO user_role (user_id, role_id) VALUES (:user_id, :role_id)";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate,
                          UserRowMapper userRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
    }

    public User getUserById(int user_id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", user_id);

        return jdbcTemplate.queryForObject(SQL_GET_BY_ID, map, userRowMapper);
    }

    public int saveUser(User user) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("email", user.getEmail())
                .addValue("password_hash", user.getPasswordHash())
                .addValue("first_name", user.getFirstName())
                .addValue("last_name", user.getLastName())
                .addValue("phone", user.getPhone());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_SAVE_USER,
                sqlParameterSource,
                keyHolder, new String[]{"id"});
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    public User getUserByEmail(String email) {
        try {
            return jdbcTemplate.getJdbcTemplate()
                    .queryForObject(SQL_GET_USER, userRowMapper, email);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void assignUserRole(User user, Role role) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", user.getId())
                .addValue("role_id", role.getId());

        jdbcTemplate.update(SQL_ASSIGN_ADMIN, map);
    }
}
