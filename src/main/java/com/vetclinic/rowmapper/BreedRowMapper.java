package com.vetclinic.rowmapper;

import com.vetclinic.object.Breed;
import com.vetclinic.repository.KindOfPetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BreedRowMapper implements RowMapper<Breed> {

    private final KindOfPetRepository kindOfPetRepository;

    @Autowired
    public BreedRowMapper(KindOfPetRepository kindOfPetRepository) {
        this.kindOfPetRepository = kindOfPetRepository;
    }

    @Override
    public Breed mapRow(ResultSet rs, int rowNum) throws SQLException {
        Breed breed = new Breed();
        breed.setId(rs.getInt("id"));
        breed.setKindOfPet(kindOfPetRepository.getKindOfPetById(rs.getInt("kind_of_pet_id")));
        breed.setName(rs.getString("name"));

        return breed;
    }
}
