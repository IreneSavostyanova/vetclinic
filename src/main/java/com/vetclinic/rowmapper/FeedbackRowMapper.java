package com.vetclinic.rowmapper;

import com.vetclinic.object.Feedback;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FeedbackRowMapper implements RowMapper<Feedback> {

    private final UserRepository userRepository;

    @Autowired
    public FeedbackRowMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
        Feedback feedback = new Feedback();
        feedback.setId(rs.getInt("id"));
        feedback.setDate(rs.getString("date"));
        feedback.setRank(rs.getInt("rank"));
        feedback.setText(rs.getString("text"));
        feedback.setUser(userRepository.getUserById(rs.getInt("user_id")));

        return feedback;
    }
}
