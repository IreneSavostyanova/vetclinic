package com.vetclinic.rowmapper;

import com.vetclinic.object.KindOfPet;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class KindOfPetRowMapper implements RowMapper<KindOfPet> {

    @Override
    public KindOfPet mapRow(ResultSet rs, int rowNum) throws SQLException {
        KindOfPet kindOfPet = new KindOfPet();
        kindOfPet.setId(rs.getInt("id"));
        kindOfPet.setName(rs.getString("name"));
        
        return kindOfPet;
    }
}
