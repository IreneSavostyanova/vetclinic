package com.vetclinic.rowmapper;

import com.vetclinic.object.KindOfService;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class KindOfServiceRowMapper implements RowMapper<KindOfService> {

    @Override
    public KindOfService mapRow(ResultSet rs, int rowNum) throws SQLException {
        KindOfService kindOfService = new KindOfService();
        kindOfService.setId(rs.getInt("id"));
        kindOfService.setName(rs.getString("name"));
        kindOfService.setDescription(rs.getString("description"));

        return kindOfService;
    }
}
