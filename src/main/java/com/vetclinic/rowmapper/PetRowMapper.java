package com.vetclinic.rowmapper;

import com.vetclinic.object.Pet;
import com.vetclinic.repository.BreedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PetRowMapper implements RowMapper<Pet> {

    private final BreedRepository breedRepository;

    @Autowired
    public PetRowMapper(BreedRepository breedRepository) {
        this.breedRepository = breedRepository;
    }

    @Override
    public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Pet pet = new Pet();
        pet.setId(rs.getInt("id"));
        pet.setDob(rs.getString("dob"));
        pet.setNickname(rs.getString("nickname"));
        pet.setBreed(breedRepository.getBreedById(rs.getInt("breed_id")));

        return pet;
    }
}
