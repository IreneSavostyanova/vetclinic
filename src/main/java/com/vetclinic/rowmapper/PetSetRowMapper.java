package com.vetclinic.rowmapper;

import com.vetclinic.object.PetSet;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PetSetRowMapper implements RowMapper<PetSet> {

    private final UserRepository userRepository;

    @Autowired
    public PetSetRowMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public PetSet mapRow(ResultSet rs, int rowNum) throws SQLException {
        PetSet petSet = new PetSet();
        petSet.setId(rs.getInt("id"));
        petSet.setUser(userRepository.getUserById(rs.getInt("user_id")));

        return petSet;
    }
}
