package com.vetclinic.rowmapper;

import com.vetclinic.object.Reception;
import com.vetclinic.repository.RecordRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ReceptionRowMapper implements RowMapper<Reception> {

    private final UserRepository userRepository;
    private final RecordRepository recordRepository;

    @Autowired
    public ReceptionRowMapper(UserRepository userRepository, RecordRepository recordRepository) {
        this.userRepository = userRepository;
        this.recordRepository = recordRepository;
    }

    @Override
    public Reception mapRow(ResultSet rs, int rowNum) throws SQLException {
        Reception reception = new Reception();
        reception.setId(rs.getInt("id"));
        reception.setRecord(recordRepository.getRecordById(rs.getInt("record_id")));
        reception.setVeterinarian(userRepository.getUserById(rs.getInt("veterinarian_id")));
        reception.setDate(rs.getString("date"));
        reception.setTime(rs.getString("time"));
        reception.setDone(rs.getBoolean("done"));

        return reception;
    }
}
