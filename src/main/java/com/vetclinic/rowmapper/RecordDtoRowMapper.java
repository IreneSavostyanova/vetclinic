package com.vetclinic.rowmapper;

import com.vetclinic.object.dto.RecordDto;
import com.vetclinic.repository.PetRepository;
import com.vetclinic.repository.ServiceRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RecordDtoRowMapper implements RowMapper<RecordDto> {

    private final UserRepository userRepository;
    private final ServiceRepository serviceRepository;
    private final PetRepository petRepository;

    @Autowired
    public RecordDtoRowMapper(UserRepository userRepository,
                              ServiceRepository serviceRepository,
                              PetRepository petRepository) {

        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
        this.petRepository = petRepository;
    }

    @Override
    public RecordDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        RecordDto recordDto = new RecordDto();
        recordDto.setRecordId(rs.getInt("id"));
        recordDto.setUser(userRepository.getUserById(rs.getInt("user_id")));
        recordDto.setServices(serviceRepository.getServicesByRecord(rs.getInt("id")));
        recordDto.setPet(petRepository.getPetById(rs.getInt("pet_id")));

        return recordDto;
    }
}
