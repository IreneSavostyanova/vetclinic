package com.vetclinic.rowmapper;

import com.vetclinic.object.Record;
import com.vetclinic.repository.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RecordRowMapper implements RowMapper<Record> {

    private final PetRepository petRepository;

    @Autowired
    public RecordRowMapper(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @Override
    public Record mapRow(ResultSet rs, int rowNum) throws SQLException {
        Record record = new Record();
        record.setId(rs.getInt("id"));
        record.setPet(petRepository.getPetById(rs.getInt("pet_id")));

        return record;
    }
}
