package com.vetclinic.rowmapper;

import com.vetclinic.object.dto.ServiceDto;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ServiceDtoRowMapper implements RowMapper<ServiceDto> {

    @Override
    public ServiceDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setId(rs.getInt("id"));
        serviceDto.setName(rs.getString("name"));
        serviceDto.setKindOfServiceName(rs.getString("kind_name"));
        serviceDto.setKindOfServiceDescription(rs.getString("description"));
        serviceDto.setPrice(rs.getDouble("price"));

        return serviceDto;
    }
}
