package com.vetclinic.rowmapper;

import com.vetclinic.object.Service;
import com.vetclinic.repository.KindOfServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ServiceRowMapper implements RowMapper<Service> {

    private final KindOfServiceRepository kindOfServiceRepository;

    @Autowired
    public ServiceRowMapper(KindOfServiceRepository kindOfServiceRepository) {
        this.kindOfServiceRepository = kindOfServiceRepository;
    }

    @Override
    public Service mapRow(ResultSet rs, int rowNum) throws SQLException {
        Service service = new Service();
        service.setId(rs.getInt("id"));
        service.setName(rs.getString("name"));
        service.setPrice(rs.getDouble("price"));
        service.setKindOfService(kindOfServiceRepository.getKindOfServiceById(rs.getInt("kind_of_service_id")));

        return service;
    }
}
