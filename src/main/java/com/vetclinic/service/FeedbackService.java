package com.vetclinic.service;

import com.vetclinic.object.Feedback;
import com.vetclinic.object.Page;
import com.vetclinic.object.PageRequest;
import com.vetclinic.repository.FeedbackRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class FeedbackService {

    private final FeedbackRepository feedbackRepository;
    private final UserRepository userRepository;
    private final DateTimeFormatter formatter;

    @Autowired
    public FeedbackService(FeedbackRepository feedbackRepository,
                           UserRepository userRepository,
                           DateTimeFormatter formatter) {

        this.feedbackRepository = feedbackRepository;
        this.userRepository = userRepository;
        this.formatter = formatter;
    }

    public void createFeedback(Feedback feedback, String userEmail) {
        feedback.setUser(userRepository.getUserByEmail(userEmail));
        feedback.setDate(LocalDateTime.now().format(formatter));
        feedbackRepository.createFeedback(feedback);
    }

    public Page<Feedback> getFeedbacks(PageRequest pageRequest) {
        Page<Feedback> page = new Page<>();
        page.setItems(feedbackRepository.getFeedbacks(pageRequest));
        page.setPage(pageRequest.getPage());
        page.setPageSize(pageRequest.getPerPage());
        int countOfFeedbacks = feedbackRepository.getCountOfFeedbacks();
        int totalCountOfPages = countOfFeedbacks / pageRequest.getPerPage();
        if (countOfFeedbacks % pageRequest.getPerPage() > 0) {
            totalCountOfPages++;
        }
        page.setTotalCountOfPages(totalCountOfPages);

        return page;
    }
}
