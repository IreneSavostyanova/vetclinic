package com.vetclinic.service;

import com.vetclinic.object.KindOfPet;
import com.vetclinic.repository.KindOfPetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KindOfPetService {

    private final KindOfPetRepository kindOfPetRepository;

    @Autowired
    public KindOfPetService(KindOfPetRepository kindOfPetRepository) {
        this.kindOfPetRepository = kindOfPetRepository;
    }

    public List<KindOfPet> getKindsOfPet() {
        return kindOfPetRepository.getKindsOfPet();
    }


}
