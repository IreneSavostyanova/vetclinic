package com.vetclinic.service;

import com.vetclinic.object.Pet;
import com.vetclinic.object.dto.PetDto;
import com.vetclinic.repository.BreedRepository;
import com.vetclinic.repository.PetRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PetService {

    private final PetRepository petRepository;
    private final BreedRepository breedRepository;
    private final UserRepository userRepository;

    @Autowired
    public PetService(PetRepository petRepository,
                      BreedRepository breedRepository,
                      UserRepository userRepository) {

        this.petRepository = petRepository;
        this.breedRepository = breedRepository;
        this.userRepository = userRepository;
    }

    public void createPetSetForUser(int userId) {
        petRepository.createPetSetForUser(userId);
    }

    @Transactional
    public void addPet(String userEmail, PetDto petDto) {
        Pet pet = getPetFromPetDto(petDto);
        pet.setId(petRepository.createPet(pet));
        petRepository.addPetToPetInSet(pet,
                petRepository.getUsersPetSet(userRepository.getUserByEmail(userEmail)));
    }

    public List<Pet> getUserPets(String userEmail) {
        return petRepository.getUserPets(userRepository.getUserByEmail(userEmail));
    }

    private Pet getPetFromPetDto(PetDto petDto) {
        Pet pet = new Pet();
        pet.setId(petDto.getId());
        pet.setNickname(petDto.getNickname());
        pet.setDob(petDto.getDob());
        pet.setBreed(breedRepository.getBreedById(petDto.getBreedId()));

        return pet;
    }

    public PetDto getPetById(int id) {
        Pet pet = petRepository.getPetById(id);
        PetDto petDto = new PetDto();
        petDto.setId(id);
        petDto.setBreedId(pet.getBreed().getId());
        petDto.setNickname(pet.getNickname());
        petDto.setDob(pet.getDob());

        return petDto;
    }

    public void updatePet(PetDto petDto) {
        petRepository.updatePet(getPetFromPetDto(petDto));
    }

    public void deletePetById(int id) {
        petRepository.deletePetById(id);
    }
}
