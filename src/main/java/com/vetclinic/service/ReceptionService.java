package com.vetclinic.service;

import com.vetclinic.object.Reception;
import com.vetclinic.object.dto.ReceptionDto;
import com.vetclinic.repository.ReceptionRepository;
import com.vetclinic.repository.RecordRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReceptionService {

    private final ReceptionRepository receptionRepository;
    private final UserRepository userRepository;
    private final RecordRepository recordRepository;

    @Autowired
    public ReceptionService(ReceptionRepository receptionRepository,
                            UserRepository userRepository,
                            RecordRepository recordRepository) {

        this.receptionRepository = receptionRepository;
        this.userRepository = userRepository;
        this.recordRepository = recordRepository;
    }

    @Transactional
    public void createReception(String userEmail, ReceptionDto receptionDto) {
        Reception reception = new Reception();
        reception.setTime(receptionDto.getTime());
        reception.setDate(receptionDto.getDate());
        reception.setVeterinarian(userRepository.getUserByEmail(userEmail));
        reception.setRecord(recordRepository.getRecordById(receptionDto.getRecordId()));
        receptionRepository.createReception(reception);
    }

    public List<ReceptionDto> getReceptionForVeterinarian(String veterinarianEmail) {
        return receptionRepository
                .getReceptionsForVeterinarian(userRepository
                        .getUserByEmail(veterinarianEmail)
                )
                .stream()
                .map(this::createReceptionDto)
                .collect(Collectors.toList());
    }

    public List<ReceptionDto> getReceptionForVeterinarianTomorrow(String veterinarianEmail) {
        return receptionRepository
                .getReceptionForVeterinarianTomorrow(userRepository
                        .getUserByEmail(veterinarianEmail)
                )
                .stream()
                .map(this::createReceptionDto)
                .collect(Collectors.toList());
    }

    public List<ReceptionDto> getReceptionForUser(String userEmail) {
        return receptionRepository
                .getReceptionForUser(userRepository
                        .getUserByEmail(userEmail)
                )
                .stream()
                .map(this::createReceptionDto)
                .collect(Collectors.toList());
    }

    public List<ReceptionDto> getReceptionsByDate(String veterinarianEmail, String date) {
        return receptionRepository.getReceptionsByDate(
                userRepository.getUserByEmail(
                        veterinarianEmail), date
        )
                .stream()
                .map(this::createReceptionDto)
                .collect(Collectors.toList());
    }

    public void markReceptionLikeDone(int id) {
        receptionRepository.markReceptionLikeDone(id);
    }

    private ReceptionDto createReceptionDto(Reception reception) {
        ReceptionDto receptionDto = new ReceptionDto();
        receptionDto.setId(reception.getId());
        receptionDto.setDate(reception.getDate());
        receptionDto.setTime(reception.getTime());
        receptionDto.setVeterinarian(reception.getVeterinarian());
        receptionDto.setRecordDto(recordRepository.getRecordDtoById(reception.getRecord().getId()));
        receptionDto.setDone(reception.isDone());

        return receptionDto;
    }
}
