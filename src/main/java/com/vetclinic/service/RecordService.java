package com.vetclinic.service;

import com.vetclinic.object.dto.RecordDto;
import com.vetclinic.repository.RecordRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecordService {

    private final RecordRepository recordRepository;
    private final UserRepository userRepository;

    @Autowired
    public RecordService(RecordRepository recordRepository,
                         UserRepository userRepository) {

        this.recordRepository = recordRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void createRecord(int petId, com.vetclinic.object.Service[] services) {
        int recordId = recordRepository.createRecord(petId);
        List<com.vetclinic.object.Service> serviceList = Arrays.stream(services).collect(Collectors.toList());
        serviceList.forEach(service -> recordRepository.addServiceInRecord(recordId, service));
    }

    public List<RecordDto> getRecords() {
        return recordRepository.getRecords();
    }
}
