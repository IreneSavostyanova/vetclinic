package com.vetclinic.service;

import com.vetclinic.object.KindOfService;
import com.vetclinic.object.dto.ServiceDto;
import com.vetclinic.repository.KindOfServiceRepository;
import com.vetclinic.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicesService {

    private final ServiceRepository serviceRepository;
    private final KindOfServiceRepository kindOfServiceRepository;

    @Autowired
    public ServicesService(ServiceRepository serviceRepository,
                           KindOfServiceRepository kindOfServiceRepository) {

        this.serviceRepository = serviceRepository;
        this.kindOfServiceRepository = kindOfServiceRepository;
    }

    public List<ServiceDto> getServices() {
        return serviceRepository.getServices();
    }

    public List<com.vetclinic.object.Service> getServicesLikeTerm(String term) {
        return serviceRepository.getServicesLikeTerm(term);
    }

    public void createService(ServiceDto service) {
        serviceRepository.createService(getServiceFromServiceDto(service));
    }

    public void updateService(ServiceDto service) {
        serviceRepository.updateService(getServiceFromServiceDto(service));
    }

    public void deleteService(int serviceId) {
        serviceRepository.deleteService(serviceId);
    }

    private com.vetclinic.object.Service getServiceFromServiceDto(ServiceDto serviceDto) {
        com.vetclinic.object.Service service = new com.vetclinic.object.Service();
        service.setKindOfService(kindOfServiceRepository.getKindOfServiceById(serviceDto.getKindOfServiceId()));
        service.setId(serviceDto.getId());
        service.setName(serviceDto.getName());
        service.setPrice(serviceDto.getPrice());

        return service;
    }

    public void updateKind(KindOfService kindOfService) {
        kindOfServiceRepository.updateKind(kindOfService);
    }

    public void createKind(KindOfService kindOfService) {
        kindOfServiceRepository.createKind(kindOfService);
    }

    public void deleteKind(int kindOfServiceId) {
        kindOfServiceRepository.deleteKind(kindOfServiceId);
    }

    public com.vetclinic.object.Service getServiceById(int id) {
        return serviceRepository.getServiceById(id);
    }

    public List<KindOfService> getKinds() {
        return kindOfServiceRepository.getKinds();
    }

    public KindOfService getKindOfServiceById(int id) {
        return kindOfServiceRepository.getKindOfServiceById(id);
    }

    public List<com.vetclinic.object.Service> getServicesReal() {
        return serviceRepository.getServicesReal();
    }
}
