package com.vetclinic.service;

import com.vetclinic.enums.Roles;
import com.vetclinic.object.Role;
import com.vetclinic.object.User;
import com.vetclinic.registration.UserRegistrationForm;
import com.vetclinic.repository.RoleRepository;
import com.vetclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Transactional
    public int register(UserRegistrationForm userRegistrationForm) {
        User user = createNewUser(userRegistrationForm);
        int id = userRepository.saveUser(user);
        Role role = roleRepository.getRoleByName(Roles.USER.toString());
        user.setRole(role);
        user.setId(id);
        roleRepository.addRoleToUser(user, role);

        return id;
    }

    private User createNewUser(UserRegistrationForm userRegistrationForm) {
        User user = new User();
        user.setEmail(userRegistrationForm.getEmail());
        user.setFirstName(userRegistrationForm.getFirstName());
        user.setLastName(userRegistrationForm.getLastName());
        user.setPasswordHash(passwordEncoder.encode(userRegistrationForm.getPassword()));
        user.setPhone(userRegistrationForm.getPhone());

        return user;
    }

    public void assignUserAdmin(int userId) {
        Role role = roleRepository.getRoleByName(Roles.ADMIN.toString());
        userRepository.assignUserRole(userRepository.getUserById(userId), role);
    }

    public void assignUserVet(int userId) {
        Role role = roleRepository.getRoleByName(Roles.VETERINARIAN.toString());
        userRepository.assignUserRole(userRepository.getUserById(userId), role);
    }
}
