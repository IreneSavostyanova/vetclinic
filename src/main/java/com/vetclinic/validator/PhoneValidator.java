package com.vetclinic.validator;

import com.vetclinic.validator.annotation.Phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PhoneValidator implements ConstraintValidator<Phone, String> {

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext context) {
        Pattern regex = Pattern.compile("^([0-9]{3}|0|\\+[0-9]{3})([0-9]{2})[0-9]{7}$");
        Matcher m = regex.matcher(phone);

        return m.matches();
    }
}
