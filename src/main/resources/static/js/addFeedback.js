function addFeedback() {
    $("#add-comment").append(
        "<div class='form-group'>" +
        "<label class='col-sm-2 control-label'>Feedback</label>" +
        "<div class='col-sm-10'>" +
        "<textarea class='form-control' name='text' id='text' rows='5'></textarea></div>\n" +
        "</div>" +
        "<div class='form-group'>" +
        "<label class='col-sm-2 control-label'>Rank</label>" +
        "<input type='text' value='1' id='rank' name='rank'>" +
        "</div>" +
        "</div>" +
        "<div class='form-group'>" +
        "<div class='col-sm-offset-2 col-sm-10'>" +
        "<button class='btn btn-success btn-circle text-uppercase' onclick='submitFeedback()'>" +
        "<span class='glyphicon glyphicon-send'></span>Submit feedback</button>" +
        "</div>" +
        "</div>"
    );
}