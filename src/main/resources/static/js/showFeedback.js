function showFeedbacks(page) {
    alert("s")
    $.ajax({
        type: "GET",
        url: "/vet_clinic/get_feedbacks?page=" + page,
        success(feedbackPage) {
            $("#containerFeedbacks").empty();
            $.each(feedbackPage.items, function (i, feedback) {
                $("#containerFeedbacks").append(
                    "<div class='well well-lg'>" +
                    "<h4 class='media-heading text-uppercase reviews'>" +
                    "" + feedback.user.firstName + ' ' + feedback.user.lastName + " </h4>" +
                    "<p class='media-comment'>" + feedback.text + "</p><br/>" +
                    " RANK DATE</div>"
                );
            });
            var previousPage = page - 1;
            if (previousPage === 0) {
                previousPage = page;
            }
            var nextPage = page + 1;
            if (nextPage > feedbackPage.totalCountOfPages) {
                nextPage = page;
            }
            $("#pagination").empty();
            $("#pagination").append(
                "<ul class='pagination'>" +
                "<li class='page-item'>" +
                "<span class='page-link' onclick='showFeedbacks(" + previousPage + ")'>Previous</span>" +
                "</li>" +
                "<div id='forPages' style='display: flex'></div>" +
                "<li class='page-item'>" +
                "<a class='page-link' onclick='showFeedbacks(" + nextPage + ")'>Next</a>" +
                "</li>" +
                "</ul>"
            );
            $("#forPages").empty();
            for (let i = 1; i <= feedbackPage.totalCountOfPages; i++) {
                $("#forPages").append(
                    "<li id='page" + i + "' class='page-item' id = 'numPages'>" +
                    "<a class='page-link' onclick='showFeedbacks(" + i + ")'>" + i + "</a>" +
                    "</li>"
                );
            }
        }
    });
}