function submitFeedback() {
    let text = $("#text").val();
    let rank = $("#rank").val();
    let feedback = {text: text, rank: rank};
    $.ajax({
        type: "POST",
        url: "/vet_clinic/add_feedback",
        data: JSON.stringify(feedback),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error() {
            $("#containerFeedbacks").empty();
            showFeedbacks(1);
            addFeedback();
        }
    });
}